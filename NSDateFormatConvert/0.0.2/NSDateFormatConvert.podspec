#
# Be sure to run `pod lib lint NSDateFormatConvert.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NSDateFormatConvert'
  s.version          = '0.0.2'
  s.summary          = '将NSDate转换为文字描述'

  s.description      = <<-DESC
将NSDate转换为文字描述，1分钟之前，2天以前。。。。
                       DESC

  s.homepage         = 'https://bitbucket.org/runnercoder/nsdateformatconvert'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'starrykai' => 'starryskykai@gmail.com' }
  s.source           = { :git => 'https://runnercoder@bitbucket.org/runnercoder/nsdateformatconvert.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'NSDateFormatConvert/Classes/*.{h,m}'
  s.frameworks = 'Foundation'
end
