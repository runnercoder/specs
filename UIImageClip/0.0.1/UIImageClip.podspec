#
# Be sure to run `pod lib lint UIImageClip.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UIImageClip'
  s.version          = '0.0.1'
  s.summary          = '图片裁剪'

  s.description      = <<-DESC
裁剪图片，根据颜色生成纯色图片
                       DESC

  s.homepage         = 'https://bitbucket.org/runnercoder/uiimageclip'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'starrykai' => 'starryskykai@gmail.com' }
  s.source           = { :git => 'https://runnercoder@bitbucket.org/runnercoder/uiimageclip.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'UIImageClip/Classes/*.{h, m}'
  s.frameworks = 'UIKit'
end
