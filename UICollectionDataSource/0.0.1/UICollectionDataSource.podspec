#
# Be sure to run `pod lib lint UICollectionDataSource.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UICollectionDataSource'
  s.version          = '0.0.1'
  s.summary          = 'UICollectionViewDataSource'

  s.description      = <<-DESC
A Class implemented UICollectionDataSource ,for the UIViewController clean code
                       DESC

  s.homepage         = 'https://bitbucket.org/runnercoder/uicollectionviewdatasource'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'starrykai' => 'starryskykai@gmail.com' }
  s.source           = { :git => 'https://runnercoder@bitbucket.org/runnercoder/uicollectionviewdatasource.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'UICollectionDataSource/Classes/*.{h, m}'
  s.frameworks = 'UIKit'
end
