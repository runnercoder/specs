#
# Be sure to run `pod lib lint UITableViewDataSource.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UITableViewDataSource'
  s.version          = '0.0.1'
  s.summary          = 'UITableView DataSource'

  s.description      = <<-DESC
The Class implemented the UITableViewDataSource, for the UIViewController clean code
                       DESC

  s.homepage         = 'https://bitbucket.org/runnercoder/uitableviewdatasource'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'starrykai' => 'starryskykai@gmail.com' }
  s.source           = { :git => 'https://runnercoder@bitbucket.org/runnercoder/uitableviewdatasource.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'UITableViewDataSource/Classes/*.{h, m}'
  s.frameworks = 'UIKit'
end
