#
# Be sure to run `pod lib lint NSStringRegex.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NSStringRegex'
  s.version          = '0.0.1'
  s.summary          = '字符串格式验证'

  s.description      = <<-DESC
使用正则表达式验证字符串格式
                       DESC

  s.homepage         = 'https://bitbucket.org/runnercoder/nsstringregex'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'starrykai' => 'starryskykai@gmail.com' }
  s.source           = { :git => 'https://runnercoder@bitbucket.org/runnercoder/nsstringregex.git', :tag => "#{s.version}" }
  s.ios.deployment_target = '8.0'
  s.source_files = 'NSStringRegex/Classes/*.{h,m}'
  s.frameworks = 'Foundation'
  s.requires_arc = true
end
